/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab5;

/**
 *
 * @author 16476
 */

enum Weight {
    KILOGRAMS,POUNDS;
}

enum Height {
    Inches, Centimeters;
}

public class Person {

    private String name;
    private double weight, height;

    public Person(String name, double w, double h ){
    
        this.name=name;
        this.weight=w;
        this.height=h;
    }

    public double bmikm(){
        return weight/(height*height);
    }

    public double bmipm(){
        return (0.4535*weight)/(height*height);
    }

    public double bmiki(){
        return weight/((0.0254*height)*(0.0254*height));
    }

    public double bmili(){
        return (0.4535*weight)/((0.0254*height)*(0.0254*height));
    }

    public String getName(){
        return name;
    }

    public double getWeight(){
        return weight;
    }

    public double getHeight(){
        return height;
    }
}